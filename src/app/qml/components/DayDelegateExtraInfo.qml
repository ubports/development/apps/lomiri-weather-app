/*
 * Copyright (C) 2015 Canonical Ltd.
 *
 * This file is part of Lomiri Weather App
 *
 * Lomiri Weather App is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * Lomiri Weather App is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.12
import Lomiri.Components 1.3


Column {
    id: extraInfoColumn
    anchors {
        top: parent.top
        // do not add bottom anchor here
    }
    width: parent.width
    objectName: "dayDelegateExtraInfo"
    spacing: units.gu(1.5)

    property bool isToday: checkToday(modelData.date)

    // Overview text
    Label {
        id: conditionForecast
        color: theme.palette.normal.baseText
        fontSize: "large"
        horizontalAlignment: Text.AlignHCenter
        text: modelData.condition
        width: parent.width
    }

    ForecastDetailsDelegate {
        id: chanceOfPrecipForecast
        forecast: i18n.tr("Chance of precipitation")
        imageSource: "qrc:/extended-information_chance-of-sleet.svg"
        // sometimes even with zero downfall, chance of precipitation returns a (small) value
        // 10% chance for zero downfall does not make much sense
        // -> disable entry for empty rain/snow values by setting value to ""
        value: modelData.rain > 0 || modelData.snow > 0 ? "" : modelData.chanceOfPrecip
    }

    ForecastDetailsDelegate {
        id: rainForecast
        forecast: i18n.tr("Rain volume")
        imageSource: "qrc:/extended-information_chance-of-rain.svg"
        value: modelData.rain
    }

    ForecastDetailsDelegate {
        id: snowForecast
        forecast: i18n.tr("Snow volume")
        imageSource: "qrc:/extended-information_chance-of-snow.svg"
        value: modelData.snow
    }

    ForecastDetailsDelegate {
        id: windForecast
        forecast: i18n.tr("Winds")
        imageSource: "qrc:/extended-information_wind.svg"
        objectName: "windForecast"
        value: isToday ? "" : modelData.wind //disable entry for today's entry
    }

    ForecastDetailsDelegate {
        id: uvIndexForecast
        imageSource: "qrc:/extended-information_uv-level.svg"
        forecast: i18n.tr("UV Index")
        value: modelData.uvIndexDay
    }

    ForecastDetailsDelegate {
        id: sunshineDurationForecast
        imageName: "weather-clear-symbolic"
        forecast: i18n.tr("Sunshine duration")
        value: modelData.sunshinetime
    }

    ForecastDetailsDelegate {
        id: humidityForecast
        forecast: i18n.tr("Humidity")
        imageSource: "qrc:/extended-information_humidity.svg"
        value: isToday ? "" : modelData.humidity //disable entry for today's entry
    }

    ForecastDetailsDelegate {
        id: sunriseForecast
        forecast: i18n.tr("Sunrise")
        imageSource: "qrc:/extended-information_sunrise.svg"
        value: modelData.sunrise
    }

    ForecastDetailsDelegate {
        id: sunsetForecast
        forecast: i18n.tr("Sunset")
        imageSource: "qrc:/extended-information_sunset.svg"
        value: modelData.sunset
    }

    ForecastDetailsDelegate {
        id: pressureForecast
        forecast: i18n.tr("Pressure")
        value: modelData.pressure
        imageSource: "qrc:/extended-information_pressure.svg"
    }

    ForecastDetailsDelegate {
        id: moonphaseForecast
        forecast: i18n.tr("Moonphase")
        imageSource: modelData.moonImage
        value: modelData.moonPhase
    }

    // checks if data day is equal to today for skipping some values
    function checkToday(datadate) {
      var day = Qt.formatDate(new Date(datadate.year, datadate.month, datadate.date, datadate.hours, datadate.minutes))
      var today = Qt.formatDate(new Date())
      return today === day ? true : false
    }
}
