/*
 * Copyright (C) 2020 UBports
 *
 * This file is part of Ubuntu Weather App
 *
 * Ubuntu Weather App is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * Ubuntu Weather App is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.12
import Lomiri.Components 1.3
import "../../components"

Item {
    id: unitsPage
    height: snowSetting.height

    ListModel {
        id: snowUnitModel
        function initialize() {
            // TRANSLATORS: millimeter, metric unit for rain/snow given as millimeter per hour, only use abbreviated
            snowUnitModel.append({"text": i18n.tr("mm"), "value": "mm"})

            // TRANSLATORS: inch, imperial unit for rain/snow given as inch per hour, only use abbreviated
            snowUnitModel.append({"text": i18n.tr("in"), "value": "in"})

            // TRANSLATORS: centimeter, metric unit for snow given as centimeter snow height, only use abbreviated
            snowUnitModel.append({"text": i18n.tr("cm"), "value": "cm"})
        }
    }

    OptionSelector {
        id: snowSetting
        text: i18n.tr("Snow volume unit")
        model: snowUnitModel
        containerHeight: itemHeight * snowUnitModel.count
        delegate: OptionSelectorDelegate {
            text: model.text
            height: units.gu(4)
        }
        onDelegateClicked: {
            settings.snowUnit = model.get(index).value
            refreshData(true);
        }
        Component.onCompleted: {
            /*
            The Component.onCompleted of the OptionSelector finishes BEFORE
            the onCompleted events of the delegates or the model.
            That is why the initialize() needs to be called here rather than
            in the Component.onCompleted of the ListModel.
            */
            snowUnitModel.initialize()
            for (var i = 0; i < snowUnitModel.count; ++i) {
                if (snowUnitModel.get(i).value === settings.snowUnit) {
                    snowSetting.selectedIndex = i
                    return
                }
            }
            snowSetting.selectedIndex = 0  // in case no match is found due to broken settings
        }
    }
}
