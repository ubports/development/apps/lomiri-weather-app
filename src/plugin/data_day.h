/*
 * Copyright (C) 2024 Maciej Sopyło <me@klh.io>
 *
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This file is part of Lomiri Weather App
 *
 * Lomiri Weather App is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * Lomiri Weather App is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef DATA_DAY_H
#define DATA_DAY_H

#include <QObject>
#include <QDateTime>
#include <QVariant>
#include <QVariantList>
#include <QList>

#include "data_point.h"

class DataDay
{
    Q_GADGET

    Q_PROPERTY(DataPoint data MEMBER m_data CONSTANT)
    Q_PROPERTY(QVariantList hourly MEMBER m_hourly CONSTANT)

public:
    DataDay(){};
    DataDay(DataPoint data, QList<DataPoint> hourly);

private:
    DataPoint m_data;
    QVariantList m_hourly;
};

#endif
