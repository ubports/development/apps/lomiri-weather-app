/*
 * Copyright (C) 2024 Maciej Sopyło <me@klh.io>
 *
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This file is part of Lomiri Weather App
 *
 * Lomiri Weather App is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * Lomiri Weather App is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <QObject>
#include <QDebug>
#include <QDataStream>
#include <QTimeZone>

#include "open_meteo_weather_provider.h"
#include "open_meteo/weather_api_generated.h"

OpenMeteoWeatherProvider::OpenMeteoWeatherProvider()
{
    connect(&m_networkAccessManager, &QNetworkAccessManager::finished, this,
            &OpenMeteoWeatherProvider::onResponse);
}

void OpenMeteoWeatherProvider::getData(QList<QGeoCoordinate> locations, QVariantList userData)
{
    QStringList latitudes;
    QStringList longitudes;
    for (const auto &location : locations) {
        latitudes.append(QString::number(location.latitude()));
        longitudes.append(QString::number(location.longitude()));
    }

    QUrlQuery query;
    query.addQueryItem("format", "flatbuffers");
    query.addQueryItem("latitude", latitudes.join(','));
    query.addQueryItem("longitude", longitudes.join(','));
    query.addQueryItem(
            "current",
            "temperature_2m,apparent_temperature,relative_humidity_2m,is_day,precipitation,rain,"
            "showers,snowfall,precipitation_probability,wind_gusts_10m,visibility,"
            "weather_code,cloud_cover,surface_pressure,wind_speed_10m,wind_direction_10m,relative_"
            "humidity_2m,uv_index");
    query.addQueryItem("hourly",
                       "temperature_2m,relative_humidity_2m,apparent_temperature,precipitation_"
                       "probability,precipitation,rain,showers,snowfall,snow_depth,weather_code,"
                       "surface_pressure,cloud_cover,wind_speed_10m,wind_direction_10m,is_day,"
                       "sunshine_duration");
    query.addQueryItem("daily",
                       "weather_code,temperature_2m_max,temperature_2m_min,apparent_temperature_"
                       "min,apparent_temperature_max,precipitation_sum,sunshine_duration,"
                       "rain_sum,showers_sum,snowfall_sum,precipitation_probability_max,wind_speed_"
                       "10m_max,wind_direction_10m_dominant,uv_index_max");
    query.addQueryItem("forecast_days", "10"); // up to 16
    query.addQueryItem("temperature_unit", "celsius");
    query.addQueryItem("wind_speed_unit", "ms");
    query.addQueryItem("precipitation_unit", "mm");
    auto tz = QTimeZone::systemTimeZoneId();
    query.addQueryItem("timezone", tz);

    QUrl url(QString("https://api.open-meteo.com/v1/forecast"));
    url.setQuery(query);

    QNetworkRequest req(url);
    req.setAttribute(QNetworkRequest::User, userData);
    m_networkAccessManager.get(req);
}

// TODO: possibly handle int64_t
void setDataFromVariable(DataPoint &dataPoint,
                         const std::unique_ptr<openmeteo_sdk::VariableWithValuesT> &entry,
                         float value)
{
    switch (entry->variable) {
        // for logging values for troubleshooting use:         qDebug() << "text: " << value; 
        case openmeteo_sdk::Variable_cloud_cover:
            dataPoint.setCloudCover(value);
            break;
        case openmeteo_sdk::Variable_apparent_temperature:
            if (entry->aggregation == openmeteo_sdk::Aggregation::Aggregation_minimum) {
                dataPoint.setFeelsLikeMin(value);
            } else if (entry->aggregation == openmeteo_sdk::Aggregation::Aggregation_maximum) {
                dataPoint.setFeelsLikeMax(value);
            } else {
                dataPoint.setFeelsLike(value);
            }
            break;
        case openmeteo_sdk::Variable_is_day:
            dataPoint.setIsDay(value == 1.0);
            break;
        case openmeteo_sdk::Variable_precipitation:
            dataPoint.setPrecipitation(value);
            break;
        case openmeteo_sdk::Variable_precipitation_probability:
            dataPoint.setPrecipitationProbability(value);
            break;
        case openmeteo_sdk::Variable_rain:
            dataPoint.setRain(value);
            break;
        case openmeteo_sdk::Variable_showers:
            dataPoint.setShowers(value);
            break;
        case openmeteo_sdk::Variable_snow_depth:
            dataPoint.setSnowDepth(value);
            break;
        case openmeteo_sdk::Variable_snowfall:
            dataPoint.setSnowfall(value);
            break;
        case openmeteo_sdk::Variable_temperature:
            if (entry->aggregation == openmeteo_sdk::Aggregation::Aggregation_minimum) {
                dataPoint.setTemperatureMin(value);
            } else if (entry->aggregation == openmeteo_sdk::Aggregation::Aggregation_maximum) {
                dataPoint.setTemperatureMax(value);
            } else {
                dataPoint.setTemperature(value);
            }
            break;
        case openmeteo_sdk::Variable_wind_speed:
            dataPoint.setWindSpeed(value);
            break;
        case openmeteo_sdk::Variable_wind_direction:
            dataPoint.setWindAzimuth(value);
            if (value >= 337.5 || value < 22.5) {
                dataPoint.setWindDirection(DataPoint::Direction::North);
            } else if (value >= 22.5 && value < 67.5) {
                dataPoint.setWindDirection(DataPoint::Direction::NorthEast);
            } else if (value >= 67.5 && value < 112.5) {
                dataPoint.setWindDirection(DataPoint::Direction::East);
            } else if (value >= 112.5 && value < 157.5) {
                dataPoint.setWindDirection(DataPoint::Direction::SouthEast);
            } else if (value >= 157.5 && value < 202.5) {
                dataPoint.setWindDirection(DataPoint::Direction::South);
            } else if (value >= 202.5 && value < 247.5) {
                dataPoint.setWindDirection(DataPoint::Direction::SouthWest);
            } else if (value >= 247.5 && value < 292.5) {
                dataPoint.setWindDirection(DataPoint::Direction::West);
            } else if (value >= 292.5 && value < 337.5) {
                dataPoint.setWindDirection(DataPoint::Direction::NorthWest);
            }
            break;
        case openmeteo_sdk::Variable_weather_code:
            dataPoint.setWeatherCode(static_cast<DataPoint::WeatherCode>(static_cast<uint8_t>(value)));
            break;
        case openmeteo_sdk::Variable_relative_humidity:
            dataPoint.setHumidity(value);
            break;
        case openmeteo_sdk::Variable_surface_pressure:
            dataPoint.setPressure(value);
            break;

        case openmeteo_sdk::Variable_precipitation_hours:
        case openmeteo_sdk::Variable_pressure_msl:
        case openmeteo_sdk::Variable_runoff:
        case openmeteo_sdk::Variable_sensible_heat_flux:
        case openmeteo_sdk::Variable_shortwave_radiation:
        case openmeteo_sdk::Variable_shortwave_radiation_instant:
        case openmeteo_sdk::Variable_snow_height:
        case openmeteo_sdk::Variable_snowfall_height:
        case openmeteo_sdk::Variable_snowfall_water_equivalent:
        case openmeteo_sdk::Variable_sunrise:
        case openmeteo_sdk::Variable_sunset:
        case openmeteo_sdk::Variable_soil_moisture:
        case openmeteo_sdk::Variable_soil_moisture_index:
        case openmeteo_sdk::Variable_soil_temperature:
        case openmeteo_sdk::Variable_surface_temperature:
        case openmeteo_sdk::Variable_terrestrial_radiation:
        case openmeteo_sdk::Variable_terrestrial_radiation_instant:
        case openmeteo_sdk::Variable_total_column_integrated_water_vapour:
        case openmeteo_sdk::Variable_updraft:
        case openmeteo_sdk::Variable_uv_index:
            if (entry->aggregation == openmeteo_sdk::Aggregation::Aggregation_maximum) {
                // for daily value the uv index with aggregation set to max is returned by the flatbuffer response
                // see https://github.com/open-meteo/sdk/issues/148#issuecomment-2425984453
                dataPoint.setUvIndexDay(value);
            } else {
                dataPoint.setUvIndex(value);
            }
            break;
        case openmeteo_sdk::Variable_uv_index_clear_sky:
        case openmeteo_sdk::Variable_vapour_pressure_deficit:
        case openmeteo_sdk::Variable_visibility:
            dataPoint.setVisibility(value);
            break;
        case openmeteo_sdk::Variable_wind_gusts:
            dataPoint.setWindGusts(value);
            break;
        case openmeteo_sdk::Variable_vertical_velocity:
        case openmeteo_sdk::Variable_geopotential_height:
        case openmeteo_sdk::Variable_wet_bulb_temperature:
        case openmeteo_sdk::Variable_river_discharge:
        case openmeteo_sdk::Variable_wave_height:
        case openmeteo_sdk::Variable_wave_period:
        case openmeteo_sdk::Variable_wave_direction:
        case openmeteo_sdk::Variable_wind_wave_height:
        case openmeteo_sdk::Variable_wind_wave_period:
        case openmeteo_sdk::Variable_wind_wave_peak_period:
        case openmeteo_sdk::Variable_wind_wave_direction:
        case openmeteo_sdk::Variable_swell_wave_height:
        case openmeteo_sdk::Variable_swell_wave_period:
        case openmeteo_sdk::Variable_swell_wave_peak_period:
        case openmeteo_sdk::Variable_swell_wave_direction:
        case openmeteo_sdk::Variable_pm10:
        case openmeteo_sdk::Variable_pm2p5:
        case openmeteo_sdk::Variable_dust:
        case openmeteo_sdk::Variable_aerosol_optical_depth:
        case openmeteo_sdk::Variable_carbon_monoxide:
        case openmeteo_sdk::Variable_nitrogen_dioxide:
        case openmeteo_sdk::Variable_ammonia:
        case openmeteo_sdk::Variable_ozone:
        case openmeteo_sdk::Variable_sulphur_dioxide:
        case openmeteo_sdk::Variable_alder_pollen:
        case openmeteo_sdk::Variable_birch_pollen:
        case openmeteo_sdk::Variable_grass_pollen:
        case openmeteo_sdk::Variable_mugwort_pollen:
        case openmeteo_sdk::Variable_olive_pollen:
        case openmeteo_sdk::Variable_ragweed_pollen:
        case openmeteo_sdk::Variable_european_aqi:
        case openmeteo_sdk::Variable_european_aqi_pm2p5:
        case openmeteo_sdk::Variable_european_aqi_pm10:
        case openmeteo_sdk::Variable_european_aqi_nitrogen_dioxide:
        case openmeteo_sdk::Variable_european_aqi_ozone:
        case openmeteo_sdk::Variable_european_aqi_sulphur_dioxide:
        case openmeteo_sdk::Variable_us_aqi:
        case openmeteo_sdk::Variable_us_aqi_pm2p5:
        case openmeteo_sdk::Variable_us_aqi_pm10:
        case openmeteo_sdk::Variable_us_aqi_nitrogen_dioxide:
        case openmeteo_sdk::Variable_us_aqi_ozone:
        case openmeteo_sdk::Variable_us_aqi_sulphur_dioxide:
        case openmeteo_sdk::Variable_us_aqi_carbon_monoxide:
        case openmeteo_sdk::Variable_sunshine_duration:
            dataPoint.setSunshineDuration(value);
            break;
        case openmeteo_sdk::Variable_convective_inhibition:
        case openmeteo_sdk::Variable_shortwave_radiation_clear_sky:
        case openmeteo_sdk::Variable_global_tilted_irradiance:
        case openmeteo_sdk::Variable_global_tilted_irradiance_instant:
        case openmeteo_sdk::Variable_undefined:
        case openmeteo_sdk::Variable_cape:
        case openmeteo_sdk::Variable_cloud_cover_high:
        case openmeteo_sdk::Variable_cloud_cover_low:
        case openmeteo_sdk::Variable_cloud_cover_mid:
        case openmeteo_sdk::Variable_daylight_duration:
        case openmeteo_sdk::Variable_dew_point:
        case openmeteo_sdk::Variable_diffuse_radiation:
        case openmeteo_sdk::Variable_diffuse_radiation_instant:
        case openmeteo_sdk::Variable_direct_normal_irradiance:
        case openmeteo_sdk::Variable_direct_normal_irradiance_instant:
        case openmeteo_sdk::Variable_direct_radiation:
        case openmeteo_sdk::Variable_direct_radiation_instant:
        case openmeteo_sdk::Variable_et0_fao_evapotranspiration:
        case openmeteo_sdk::Variable_evapotranspiration:
        case openmeteo_sdk::Variable_freezing_level_height:
        case openmeteo_sdk::Variable_growing_degree_days:
        case openmeteo_sdk::Variable_latent_heat_flux:
        case openmeteo_sdk::Variable_leaf_wetness_probability:
        case openmeteo_sdk::Variable_lifted_index:
        case openmeteo_sdk::Variable_lightning_potential:

        break;
    }
}

void OpenMeteoWeatherProvider::onResponse(QNetworkReply *reply)
{
    QVariant userData = reply->request().attribute(QNetworkRequest::User);
    if (!userData.canConvert<QVariantList>()) {
        qDebug() << "userData is not a list!" << userData;
        emit error("userData is not a list", userData);
        return;
    }
    QVariantList userDataList = userData.toList();

    if (reply->error() != QNetworkReply::NoError) {
        qDebug() << reply->errorString();
        emit error(reply->errorString(), QVariant());
        return;
    }

    size_t i = 0;
    while (reply->bytesAvailable() > 0) {
        QByteArray sizeBytes = reply->read(4);
        if (sizeBytes.size() != 4) {
            qDebug() << "can't read flatbuffer size" << sizeBytes;
            emit error("received incorrect data from the server", QVariant());
            return;
        }

        QDataStream sizeStream(sizeBytes);
        sizeStream.setByteOrder(QDataStream::LittleEndian);
        uint32_t size;
        sizeStream >> size;

        QByteArray message = reply->read(size);
        if (message.size() != size) {
            qDebug() << "flatbuffer size incorrect" << message.size() << "expected" << size;
            emit error("received incorrect data from the server", QVariant());
            return;
        }

        handleResponseChunk(message, userDataList[i]);
        i++;
    }

    if (i < userDataList.size()) {
        qDebug() << "returned less locations than requested!";
    }
}

void OpenMeteoWeatherProvider::handleResponseChunk(QByteArray bytes, QVariant userData)
{
    flatbuffers::Verifier verifier((const uint8_t *)bytes.constData(),
                                   (const size_t)bytes.length());
    bool ok = openmeteo_sdk::VerifyWeatherApiResponseBuffer(verifier);
    if (!ok) {
        qDebug() << "error in flatbuffer data!";
        emit error("received incorrect data from the server", QVariant());
        return;
    }

    openmeteo_sdk::WeatherApiResponseT res;
    openmeteo_sdk::GetWeatherApiResponse(bytes.constData())->UnPackTo(&res);

    if (!res.current) {
        // if the result doesn't even have current weather something is wrong
        qDebug() << "forecast has no current weather data";
        emit error("received incorrect data from the server", QVariant());
        return;
    }

    // TODO: we could use `time_end` to refetch data / mark stale?
    DataPoint current(QDateTime::fromSecsSinceEpoch(res.current->time));
    for (const auto &variable : res.current->variables) {
        setDataFromVariable(current, variable, variable->value);
    }

    QList<DataDay> forecast;

    if (res.daily) {
        forecast.reserve(res.daily->variables.size());
        for (int64_t ts = res.daily->time; ts < res.daily->time_end; ts += res.daily->interval) {
            size_t day = static_cast<size_t>((ts - res.daily->time) / res.daily->interval);

            DataPoint dayData(QDateTime::fromSecsSinceEpoch(ts));
            for (const auto &variable : res.daily->variables) {
                setDataFromVariable(dayData, variable, variable->values[day]);
            }

            QList<DataPoint> hourly;

            if (res.hourly) {
                float humidity = -1.0f;
                float pressure = -1.0f;
                hourly.reserve(24); // only one day can have less
                // not starting at `ts` because it can be desynced with hourly interval
                for (int64_t hts = res.hourly->time; hts < res.hourly->time_end;
                     hts += res.hourly->interval) {
                    if (hts < ts) {
                        continue;
                    }
                    if (hts >= ts + res.daily->interval) {
                        break;
                    }

                    size_t hour =
                            static_cast<size_t>((hts - res.hourly->time) / res.hourly->interval);

                    DataPoint point(QDateTime::fromSecsSinceEpoch(hts));
                    for (const auto &variable : res.hourly->variables) {
                        setDataFromVariable(point, variable, variable->values[hour]);
                        if (variable->variable == openmeteo_sdk::Variable_relative_humidity) {
                            humidity += variable->values[hour];
                        } else if (variable->variable == openmeteo_sdk::Variable_surface_pressure) {
                            pressure += variable->values[hour];
                        }
                    }
                    hourly.append(point);
                }

                // calculate average humidity and pressure from hourly data because the API doesn't
                // provide those for daily forecast
                if (humidity >= 0.0f) {
                    dayData.setHumidity(humidity / static_cast<float>(hourly.size()));
                }
                if (pressure >= 0.0f) {
                    dayData.setPressure(pressure / static_cast<float>(hourly.size()));
                }
            }

            forecast.append(DataDay(dayData, hourly));
        }
    }

    emit dataReceived(QGeoCoordinate(res.latitude, res.longitude), current, forecast, userData);
}
