/*
 * Copyright (C) 2024 Maciej Sopyło <me@klh.io>
 *
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This file is part of Lomiri Weather App
 *
 * Lomiri Weather App is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * Lomiri Weather App is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef PROVIDER_H
#define PROVIDER_H

#include <QObject>
#include <QDebug>
#include <QDateTime>
#include <QVariantList>
#include <QGeoCoordinate>

#include "data_point.h"
#include "data_day.h"

class Provider : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QObject *provider MEMBER m_dataProvider WRITE setProvider)
public:
    Provider();
    ~Provider() = default;

    Q_INVOKABLE bool getData(QVariantList locations, QVariantList userData);

signals:
    void dataReceived(QGeoCoordinate coordinate, DataPoint current, QVariantList forecast,
                      QVariant userData);
    void error(QString message, QVariant data);

private slots:
    void onDataReceived(QGeoCoordinate coordinate, DataPoint current, QList<DataDay> forecast,
                        QVariant userData);
    void onError(QString message, QVariant data);

private:
    QObject *m_dataProvider = nullptr;

    void setProvider(QObject *provider);
};

#endif // PROVIDER_H
